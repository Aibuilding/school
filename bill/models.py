from django.contrib import admin
from django.db import models

# Create your models here.

class bills(models.Model):
    bid = models.AutoField(primary_key=True)
    uid = models.CharField(max_length=200)
    ispay = models.CharField(max_length=200, default="0")
    type = models.CharField(max_length=200)
    price = models.CharField(max_length=200)
    createday = models.CharField(max_length=200)
    payday = models.CharField(max_length=200)

class elecrecord(models.Model):
    rid = models.AutoField(primary_key=True)
    roomid = models.CharField(max_length=40)
    roomname=models.CharField(max_length=40)
    lastdate = models.CharField(max_length=50)
    date = models.CharField(max_length=50)
    last = models.CharField(max_length=50)
    this = models.CharField(max_length=50)
    shangqian = models.CharField(max_length=50)
    shangyu = models.CharField(max_length=50)
    jiao = models.CharField(max_length=50)


admin.site.register(bills)