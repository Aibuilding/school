import json

from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from bill.models import bills, elecrecord


def getbilllist(e):
    _ispay = e.GET.get('ispay', None)
    _uid = e.GET.get('uid', None)
    _bills = bills.objects.values()
    if (_ispay):
        _billlist = _bills.filter(uid=_uid, ispay=_ispay)
    else:
        _billlist = _bills.filter(uid=_uid)
    str = ""
    for bill in _billlist:
        for name, value in bill.items():
            str += f'{value},'
        str += ';'
    return HttpResponse(str)


def pay(e):
    _bid = e.GET.get('bid', None)

    bill = bills.objects.get(bid=_bid)
    print(bill)
    bill.ispay = "1"
    bill.save()
    return HttpResponse("success")


def getelecrecord(e):
    roomid = e.GET.get('roomid', None)
    res = []
    ans = elecrecord.objects.values()
    if ans.filter(roomid=roomid):
        records = ans.filter(roomid=roomid)
        for i in records:
            res.append({"lastdate": i["lastdate"], "date": i["date"], "roomname": i["roomname"], "last": i["last"],
                        "ben": i["this"], "qian": i["shangqian"], "yu": i["shangyu"], "jiao": i["jiao"]})

    return HttpResponse(json.dumps(res))

