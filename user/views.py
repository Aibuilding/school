import json

import requests
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from user.models import userinfo


def login(e):
    ph = e.GET.get('id', None)
    js_code = ph
    appid = 'wxe3bf4126cfc7050b'
    secret = 'c66adefb7dc19387ea62680860805650'
    url = 'https://api.weixin.qq.com/sns/jscode2session?appid={APPID}&secret={SECRET}&js_code={JSCODE}&grant_type=authorization_code'.format(
        APPID=appid, SECRET=secret, JSCODE=js_code)
    r = requests.get(url)
    r = r.json()['openid']
    qs = userinfo.objects.values()
    if r:
        if qs.filter(wxid=r):
            qs = qs.filter(wxid=r)
            str = ''
            for user in qs:
                for name, value in user.items():
                    str += f'{value}/'
            return HttpResponse(str)
        else:
            t = userinfo(wxid=r, detail="无;无;无;无;请登录")
            t.save()
            qs = userinfo.objects.values()
            if qs.filter(wxid=r):
                qs = qs.filter(wxid=r)
                str = ''
                for user in qs:
                    for name, value in user.items():
                        str += f'{value}/'
                return HttpResponse(str)


def save(e):
    _uid = e.GET.get('uid', None)
    unm = e.GET.get('username', None)
    pwd = e.GET.get('password', None)
    us = userinfo.objects.values()
    if us.filter(number=unm):
        user = userinfo.objects.get(number=unm)
        if (user.password == pwd):
            _user = userinfo.objects.get(uid=_uid)
            _wxid = _user.wxid
            user.wxid = _wxid
            _user.wxid = ""
            _user.save()
            user.save()
            res = {"code": "OK", "uid": user.uid, "detail": user.detail, "classid": user.classid, "roomid": user.roomid}
        else:
            res = {"code": "NO", "message": "密码错误"}
    else:
        res = {"code": "NO", "message": "账号不存在密码错误"}
    return HttpResponse(json.dumps(res))
