from django.db import models

# Create your models here.
from django.contrib import admin


class userinfo(models.Model):
    uid = models.AutoField(primary_key=True)
    wxid = models.CharField(max_length=200)
    detail = models.CharField(max_length=200)
    number = models.CharField(max_length=200)
    classid = models.CharField(max_length=200)
    roomid = models.CharField(max_length=200)
    type = models.CharField(max_length=200)
    password = models.CharField(max_length=200)


class college(models.Model):
    cid = models.AutoField(primary_key=True)
    cname = models.CharField(max_length=200)

class major(models.Model):
    mid = models.AutoField(primary_key=True)
    collegeid=models.CharField(max_length=20)
    mname = models.CharField(max_length=200)

class classroom(models.Model):
    cid = models.AutoField(primary_key=True)
    majorid = models.CharField(max_length=20)
    cname = models.CharField(max_length=200)

class room(models.Model):
    rid = models.AutoField(primary_key=True)
    rname = models.CharField(max_length=200)

admin.site.register(userinfo)