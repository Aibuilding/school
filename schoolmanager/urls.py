"""schoolmanager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from active.views import getactive, getmessage, releasemessage
from bill.views import getbilllist, pay, elecrecord, getelecrecord
from edu.views import gettable, getedunotice, getscore, getexam
from library.views import borrowlist, search, bookdetail, getnotice, gethot
from user.views import login, save

urlpatterns = [
    path('admin/', admin.site.urls),
    path('user/login/', login),
    path('user/save/', save),
    path('bill/getbilllist/', getbilllist),
    path('bill/pay/', pay),
    path('bill/getelecrecord/', getelecrecord),
    path('library/borrowlist/', borrowlist),
    path('library/search/', search),
    path('library/bookdetail/', bookdetail),
    path('getactive/', getactive),
    path('library/getnotice/', getnotice),
    path('library/gethot/', gethot),
    path('edu/gettable/', gettable),
    path('edu/getnotice/', getedunotice),
    path('edu/getscore/', getscore),
    path('edu/getexam/', getexam),
    path('board/getmessage/', getmessage),
    path('board/releasemessage/', releasemessage),

]
