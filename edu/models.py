from django.db import models

# Create your models here.
class course(models.Model):
    cid = models.AutoField(primary_key=True)
    cnumber = models.CharField(max_length=20)
    cweight = models.CharField(max_length=5)
    cname = models.CharField(max_length=20)

class table(models.Model):
    tid = models.AutoField(primary_key=True)
    classid = models.CharField(max_length=20)
    table = models.CharField(max_length=300)

class sc(models.Model):
    scid = models.AutoField(primary_key=True)
    uid = models.CharField(max_length=20)
    cid = models.CharField(max_length=20)
    score = models.CharField(max_length=20)
class notice(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=20)
    date = models.CharField(max_length=20)
    detail = models.CharField(max_length=20)

class exam(models.Model):
    id = models.AutoField(primary_key=True)
    cid = models.CharField(max_length=20)
    classid=models.CharField(max_length=20)
    date = models.CharField(max_length=20)
    place = models.CharField(max_length=20)


