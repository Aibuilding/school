import json

from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from edu.models import table, notice, sc, course, exam


def gettable(e):
    cid = e.GET.get('cid', None)
    res = {}
    ans = table.objects.values()
    if ans.filter(classid=cid):
        tablestr = ans.filter(classid=cid)
        for i in tablestr:
            res = i['table']

    return HttpResponse(json.dumps(res))
def getedunotice(e):
    res = []
    ans = notice.objects.values()
    for i in ans:
        res.append({"title":i['title'],"date":i['date'],"detail":i['detail']})

    return HttpResponse(json.dumps(res))
def getscore(e):
    uid = e.GET.get('uid', None)
    res = []
    ans = sc.objects.values()
    if ans.filter(uid=uid):
        scores = ans.filter(uid=uid)
        for i in scores:
            c=course.objects.get(cid=i["cid"])
            res.append({"cnum": c.cnumber, "cname": c.cname, "score": i['score']})

    return HttpResponse(json.dumps(res))
def getexam(e):
    classid = e.GET.get('classid', None)
    res = []
    ans = exam.objects.values()
    if ans.filter(classid=classid):
        exams = ans.filter(classid=classid)
        for i in exams:
            c=course.objects.get(cid=i["cid"])
            res.append({"cnum": c.cnumber, "cname": c.cname, "date": i['date'], "place": i['place']})

    return HttpResponse(json.dumps(res))
