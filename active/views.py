import json
import time

from django.http import HttpResponse
from django.shortcuts import render
from active.models import active, board
from user.models import userinfo


def getactive(e):
    ph = e.GET.get('id', None)

    if (ph):
        re = active.objects.values().filter(type=ph)
        Str = ''
        for user in re:
            for name, value in user.items():
                Str += f'{value}/%#/'
            Str += '(/>'
        return HttpResponse(Str)
    else:

        re = active.objects.values().order_by('?')[:6]
        # re=active.objects.order_by('id').first()
        Str = ''
        for user in re:
            for name, value in user.items():
                Str += f'{value}/%#/'
            Str += '(/>'
        return HttpResponse(Str)


def getmessage(e):
    res = []
    us = board.objects.values().order_by('-id')
    for i in us:
        res.append({"muname": i['username'], "mtime": i['time'], "mdetail": i['detail'], })
    return HttpResponse(json.dumps(res))


def releasemessage(e):
    uid = e.GET.get('uid', None)
    detail = e.GET.get('detail', None)
    username = userinfo.objects.get(uid=uid).detail.split(';')[4]
    t = board(username=username, time=time.strftime("%Y-%m-%d %H:%M:%S"), detail=detail)
    t.save()
    res = []
    res.append({"succes": "true"})
    return HttpResponse(json.dumps(res))
