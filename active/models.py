from django.db import models

class active(models.Model):
    id = models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')
    type = models.CharField(max_length=2)
    img = models.CharField(max_length=200)
    link = models.CharField(max_length=200)
    text = models.CharField(max_length=200)

class board(models.Model):
    id = models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')
    username = models.CharField(max_length=20)
    time = models.CharField(max_length=20)
    detail = models.CharField(max_length=200)
