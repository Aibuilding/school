import json

from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from library.models import books, notice, hotsearch


def borrowlist(e):
    uid = e.GET.get('uid', None)
    us = books.objects.values()
    res = []
    if us.filter(borrowby=uid):
        book = us.filter(borrowby=uid)
        for i in book:
            print(i)
            res.append({"bookID": i['bookid'], "bookName": i['bookname'], "brrowDate": i['borrowday'],
                        "needReturnDate": i['borrowday']})
        return HttpResponse(json.dumps(res))

    return HttpResponse(json.dumps(res))


def bookdetail(e):
    isbn = e.GET.get('isbn', None)
    res = []
    us = books.objects.values()
    if us.filter(isbn=isbn):
        book = us.filter(isbn=isbn)
        for i in book:
            res.append({"num":i['booknum'],"place": i['bookplace'], "have": 1, "borrow": 1})
        return HttpResponse(json.dumps(res))


def search(e):
    type = e.GET.get('t', None)
    value = e.GET.get('v', None)
    us = books.objects.values()
    hs = hotsearch.objects.values()
    # 处理热搜
    if hs.filter(info=value):
        _hs = hotsearch.objects.get(info=value)
        _hs.searchnum=(int)(_hs.searchnum)+1
        _hs.save()
    else:
        t = hotsearch(info=value, searchnum=1)
        t.save()

    res = []
    if type == '0' and us.filter(bookname__icontains=value):
        book = us.filter(bookname__icontains=value)
        for i in book:
            res.append(
                {"bookID": i['bookid'], "bookName": i['bookname'], "bookInfo": i['bookinfo'], "bookISBN": i['isbn']})
        return HttpResponse(json.dumps(res), content_type='application/json; charset=UTF-8')
    if type == '1':
        if us.filter(bookinfo__icontains=value):
            book = us.filter(bookinfo__icontains=value)
            for i in book:
                res.append({"bookID": i['bookid'], "bookName": i['bookname'], "brrowDate": i['borrowday'],
                            "needReturnDate": i['borrowday']})
            return HttpResponse(json.dumps(res))
    if type == '2':
        if us.filter(bookname__icontains=value):
            book = us.filter(bookname__icontains=value)
            for i in book:
                res.append({"bookID": i['bookid'], "bookName": i['bookname'], "brrowDate": i['borrowday'],
                            "needReturnDate": i['borrowday']})
            return HttpResponse(json.dumps(res))

    return HttpResponse(json.dumps(res))

def getnotice(e):
    uid = e.GET.get('uid', None)
    res = []
    us = notice.objects.values()
    if us.filter(uid=uid):
        notices = us.filter(uid=uid)
        for i in notices:
            res.append({"info": i['info']})
        return HttpResponse(json.dumps(res))

    return HttpResponse(json.dumps(res))

def gethot(e):
    res = []
    us = hotsearch.objects.values().order_by('-searchnum')

    sum = 0
    for i in us:
        res.append({"info": i['info'], "num": i['searchnum']})
        sum += 1
        if sum > 10:
            break
    return HttpResponse(json.dumps(res))

