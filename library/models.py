from django.db import models

# Create your models here.
from django.contrib import admin


class books(models.Model):
    bookid = models.AutoField(primary_key=True)
    booknum = models.CharField(max_length=200)
    bookplace = models.CharField(max_length=200)
    bookname = models.CharField(max_length=200)
    isbn = models.CharField(max_length=200)
    bookinfo = models.CharField(max_length=200)
    borrowby = models.CharField(max_length=200)
    borrowday = models.CharField(max_length=200)

class hotsearch(models.Model):
    id = models.AutoField(primary_key=True)
    info = models.CharField(max_length=200)
    searchnum = models.CharField(max_length=200)


class follow(models.Model):
    id = models.AutoField(primary_key=True)
    uid = models.CharField(max_length=200)
    bookid = models.CharField(max_length=200)

class notice(models.Model):
    id = models.AutoField(primary_key=True)
    uid = models.CharField(max_length=200)
    info = models.CharField(max_length=200)


admin.site.register(books)